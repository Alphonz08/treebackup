<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tree extends Model
{
    //
    public function regions(){
        $this->belongsTo(Region::class);
    }

    public function users(){
        $this->belongsTo(User::class);
    }
}
