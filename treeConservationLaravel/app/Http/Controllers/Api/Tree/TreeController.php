<?php

namespace App\Http\Controllers\Api\Tree;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tree;


class TreeController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tree = Tree::all();
        //return response()->json(['data' => $tree], 201);
        echo json_encode($tree);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tree = new Tree;
        $tree->name = $request->input('name');
        $tree->height = $request->input('height');
        $tree->classification = $request->input('classification');
        $tree->features = $request->input('features');
        $tree->weather = $request->input('weather');
        $tree->note = $request->input('note');
        $tree->image = $request->input('image');
        $tree->save();
        //return response()->json(['data' => $tree], 201);
        echo json_encode($tree);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tree = Tree::find($id);
        //return response()->json(['data' => $tree], 200);
        echo json_encode($tree);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tree = Tree::find($id);
        $tree->name = $request->input('name');
        $tree->height = $request->input('height');
        $tree->classification = $request->input('classification');
        $tree->features = $request->input('features');
        $tree->weather = $request->input('weather');
        $tree->note = $request->input('note');
        $tree->image = $request->input('image');
        $tree->save();
        //return response()->json(['data' => $tree], 200);
        echo json_encode($tree);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tree = Tree::find($id);
        $tree->delete();
        $message = 'Arbol eliminado';
        //return response()->json(['data' => $message], 200);
        echo json_encode($message);
    }
}
