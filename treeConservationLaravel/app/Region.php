<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //
    public function trees(){
        $this->hasMany(Tree::class);
    }
}
