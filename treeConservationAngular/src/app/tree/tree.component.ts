import { Component, OnInit } from '@angular/core';
import { Tree } from '../interfaces/Tree';
import { TreesService } from '../services/trees.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnInit {

  trees: Tree[];

  constructor(private treeService: TreesService, private router: Router) {
    this.getTrees();
  }

  getTrees() {
    this.treeService.get().subscribe((data: Tree[]) => {
      this.trees = data;
    }, (error) => {
      console.log(error);
      alert('Ocurrio un error');
    });
  }

  ngOnInit() {

  }

  delete(id) {
    this.treeService.delete(id).subscribe((data) => {
      alert('Eliminado con exito');
      this.getTrees();
    }, (error) => {
      console.log(error);
    });
  }

}
