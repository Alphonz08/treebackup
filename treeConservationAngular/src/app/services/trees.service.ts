import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tree } from '../interfaces/Tree';

@Injectable({
  providedIn: 'root'
})
export class TreesService {

  Api = 'http://127.0.0.1:8000/api';

  constructor(private httpClient: HttpClient) { }

  get(){
    return this.httpClient.get(this.Api + '/trees');
  }

  save(tree: Tree){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(this.Api + '/trees', tree, {headers: headers});
  }
  
  put(tree: Tree){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.put(this.Api + '/trees/' + tree.id, tree, {headers: headers});
  }

  delete(id){
    return this.httpClient.delete(this.Api + '/trees/' + id);
  }
}
