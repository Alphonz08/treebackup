import { Component, OnInit } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.css']
})
export class AddUsersComponent implements OnInit {

  public form = {
    email: null,
    name: null,
    password: null,
    tastes: 'Hola'
  };

  public error = [];

  constructor(private Jarwis: JarwisService, private Token: TokenService, private route: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.form)
    this.Jarwis.signup(this.form).subscribe(
      data => this.handleReponse(data),
      error => console.log(error)
    );
  }

  handleReponse(data) {
    this.Token.handle(data.access_token);
    this.route.navigateByUrl('tree');
  }

  handleError(error) {
    this.error = error.error.errors;
  }

}
