import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddTreeComponent } from './add-tree/add-tree.component';
import { AddUsersComponent } from './add-users/add-users.component';
import { TreeComponent } from './tree/tree.component';
import { WeatherComponent } from './weather/weather.component';
import { ClassificationComponent } from './classification/classification.component';

import { Route, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PasswordComponent } from './password/password.component';
import { JarwisService } from './services/jarwis.service';
import { TokenService } from './services/token.service';
import { AuthService } from './services/auth.service';
import { NavbarComponent } from './navbar/navbar.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddTreeComponent,
    AddUsersComponent,
    TreeComponent,
    WeatherComponent,
    ClassificationComponent,
    LoginComponent,
    PasswordComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [JarwisService, TokenService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
