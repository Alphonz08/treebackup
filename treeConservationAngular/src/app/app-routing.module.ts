import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddTreeComponent } from './add-tree/add-tree.component';
import { AddUsersComponent } from './add-users/add-users.component';
import { TreeComponent } from './tree/tree.component';
import { WeatherComponent } from './weather/weather.component';
import { ClassificationComponent } from './classification/classification.component';

import { LoginComponent } from './login/login.component';
import { PasswordComponent } from './password/password.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'password', component: PasswordComponent},
  {path: 'add-users', component: AddUsersComponent},
  {path: 'add-tree', component: AddTreeComponent},
  {path: 'add-tree/:id', component: AddTreeComponent},
  {path: 'tree', component: TreeComponent},
  {path: 'weather', component: WeatherComponent},
  {path: 'classification', component: ClassificationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
