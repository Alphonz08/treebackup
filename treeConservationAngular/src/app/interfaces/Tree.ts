export interface Tree{
    id?: number;
    name: string;
    height: string;
    classification: string;
    features: string;
    weather: string;
    note: string;
    image: string;
    created_at?: string;
    updated_at?: string;
}