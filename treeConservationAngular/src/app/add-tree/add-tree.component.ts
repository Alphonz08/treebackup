import { Component, OnInit } from '@angular/core';
import { Tree } from '../interfaces/Tree';
import { TreesService } from '../services/trees.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-tree',
  templateUrl: './add-tree.component.html',
  styleUrls: ['./add-tree.component.css']
})
export class AddTreeComponent implements OnInit {

  tree: Tree = {
    name: null,
    height: null,
    classification: null,
    features: null,
    weather: null,
    note: null,
    image: null
  };

  id: any;
  editing: boolean = false;
  trees: Tree[];

  constructor(private treeService: TreesService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.id = this.activatedRoute.snapshot.params['id'];
    if(this.id) {
      this.editing = true;
      this.treeService.get().subscribe((data: Tree[]) => {
        this.trees = data;
        this.tree = this.trees.find((m) => {return m.id == this.id});
      }, (error) => {
        console.log(error);
      });
    } else {
      this.editing = false;
    }
  }

  ngOnInit() {
  }

  saveTree() {
    if (this.editing) {
      this.treeService.put(this.tree).subscribe((data) => {
        alert('Tree update');
        this.router.navigateByUrl('/tree');
      }, (error) => {
        console.log(error);
        alert('Ocurrio un error');
      });
    } else {
      this.treeService.save(this.tree).subscribe((data) => {
        alert('Tree save');
        this.router.navigateByUrl('/tree');
      }, (error) => {
        console.log(error);
        alert('Ocurrio un error');
      });
    }
  }

}
